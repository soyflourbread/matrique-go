package ui

import (
	"github.com/therecipe/qt/core"
	"gitlab.com/b0/matrique-go/matrix"
	"log"
)

type MessageEventModel struct {
	core.QAbstractListModel

	_ *Controller `property:"controller"`

	_ func() `constructor:"init"`

	_ *Room `property:"currentRoom"`

	_ map[int]*core.QByteArray `property:"roles"`
	_ []*Message               `property:"message"`

	_ func(interface{}) `signal:"doResetModel"`

	_ func(int) *Message `slot:"messageAt"`
	_ func(*Message)     `slot:"addMessage"`
	_ func(row int)      `slot:"removeMessage"`
}

func (m *MessageEventModel) init() {
	m.SetRoles(map[int]*core.QByteArray{
		Text:     core.NewQByteArray2("text", len("text")),
		AuthorID: core.NewQByteArray2("authorID", len("authorID")),
	})

	m.ConnectData(m.data)
	m.ConnectRowCount(m.rowCount)
	m.ConnectColumnCount(m.columnCount)
	m.ConnectRoleNames(m.roleNames)
	m.ConnectMessageAt(m.messageAt)
	m.ConnectDoResetModel(m.resetModel)
	m.ConnectAddMessage(m.addMessage)
	m.ConnectRemoveMessage(m.removeMessage)

	m.ConnectCurrentRoomChanged(func(currentRoom *Room) {
		log.Println("GO: room changed.")
	})
}

func (m *MessageEventModel) resetModel(msgsInterface interface{}) {
	msgs := msgsInterface.([]*matrix.Message)
	log.Println("Begin resetting roomlistmodel.")
	m.BeginResetModel()

	var qmlMsgs []*Message
	for _, msg := range msgs {
		qmlMsg := NewMessage(nil)
		qmlMsg.FromMessage(msg)
		qmlMsgs = append(qmlMsgs, qmlMsg)
	}
	m.SetMessage(qmlMsgs)
	m.EndResetModel()
	log.Println("Reset complete.")
}

func (m *MessageEventModel) messageAt(row int) *Message {
	return m.Message()[row]
}

func (m *MessageEventModel) data(index *core.QModelIndex, role int) *core.QVariant {
	if !index.IsValid() {
		return core.NewQVariant()
	}

	if index.Row() >= len(m.Message()) {
		return core.NewQVariant()
	}

	var r = m.Message()[index.Row()]

	switch role {
	case Text:
		return core.NewQVariant14(r.Text())

	case Name:
		return core.NewQVariant14(r.AuthorID())

	default:
		return core.NewQVariant()
	}
}

func (m *MessageEventModel) rowCount(parent *core.QModelIndex) int {
	return len(m.Message())
}

func (m *MessageEventModel) columnCount(parent *core.QModelIndex) int {
	return 1
}

func (m *MessageEventModel) roleNames() map[int]*core.QByteArray {
	return m.Roles()
}

func (m *MessageEventModel) addMessage(msg *Message) {
	m.BeginInsertRows(core.NewQModelIndex(), len(m.Message()), len(m.Message()))
	m.SetMessage(append(m.Message(), msg))
	m.EndInsertRows()
}

func (m *MessageEventModel) removeMessage(row int) {
	m.BeginRemoveRows(core.NewQModelIndex(), row, row)
	m.SetMessage(append(m.Message()[:row], m.Message()[row+1:]...))
	m.EndRemoveRows()
}
