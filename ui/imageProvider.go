package ui

import (
	"fmt"
	"log"
	"os"
	"strings"

	"io/ioutil"
	"net/http"

	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/gui"
	"github.com/therecipe/qt/qml"
	"github.com/therecipe/qt/quick"
	"gitlab.com/b0/initials-avatar"
)

type ImageProvider struct {
	core.QObject

	serverAddr string

	provider *quick.QQuickImageProvider

	_ func(addr string) `slot:"setServerAddr"`
}

func (ip *ImageProvider) Init() {
	ip.ConnectSetServerAddr(ip.setServerAddr)

	pro := quick.NewQQuickImageProvider(0, qml.QQmlImageProviderBase__ForceAsynchronousImageLoading)
	avatarDir := core.QStandardPaths_WritableLocation(core.QStandardPaths__CacheLocation) + "/avatar/"
	//avatarDir := os.TempDir() + "/matrique/avatar/"
	if _, err := os.Stat(avatarDir); os.IsNotExist(err) {
		os.MkdirAll(avatarDir, 0755)
	}
	log.Println("Avatar dir:", avatarDir)
	pro.ConnectRequestImage(func(id string, size *core.QSize, requestedSize *core.QSize) *gui.QImage {
		defer func() {
			if r := recover(); r != nil {
				fmt.Println("recovered:", r)
			}
		}()
		var i string

		if strings.HasPrefix(id, "mxc://") {
			// Parse MXC Uri.
			uri := id[6:]
			stringSlice := strings.Split(uri, "/")
			serverAddr := stringSlice[0]
			fileID := stringSlice[1]

			if _, err := os.Stat(avatarDir + fileID); err == nil {
				// path/to/whatever exists
				avatar, _ := ioutil.ReadFile(avatarDir + fileID)
				i = string(avatar)
			} else {
				out, _ := os.Create(avatarDir + fileID)
				defer out.Close()

				fileURL := ip.serverAddr + "/_matrix/media/r0/download/" + serverAddr + "/" + fileID
				resp, err := http.Get(fileURL)
				checkErr(err)
				defer resp.Body.Close()

				body, err := ioutil.ReadAll(resp.Body)
				out.Write(body)

				i = string(body)
			}
		} else {
			a := avatar.New()
			b, _ := a.DrawToBytes(string(id[0]), 128)
			i = string(b)
		}

		pix := gui.NewQPixmap()
		pix.LoadFromData(i, uint(len(i)), "", 0)
		return pix.ToImage()
	})
	ip.provider = pro
}

func (ip *ImageProvider) setServerAddr(addr string) {
	ip.serverAddr = addr
}

func (ip *ImageProvider) GetProvider() *quick.QQuickImageProvider {
	return ip.provider
}
