package ui

import (
	"github.com/therecipe/qt/core"
	"gitlab.com/b0/matrique-go/matrix"
)

type Message struct {
	core.QObject

	matrix.Message

	_ string `property:"authorID"`
	_ string `property:"text"`
}

func (m *Message) Init() {
	m.ConnectText(func() string { return m.Message.Text })
	m.ConnectSetText(func(text string) { m.Message.Text = text })
	m.ConnectAuthorID(func() string { return m.Message.AuthorID })
	m.ConnectSetAuthorID(func(authorID string) { m.Message.AuthorID = authorID })
}

func (m *Message) FromMessage(msg *matrix.Message) {
	m.SetText(msg.Text)
	m.SetAuthorID(msg.AuthorID)
}
