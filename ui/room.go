package ui

import (
	"github.com/therecipe/qt/core"
	"gitlab.com/b0/matrique-go/matrix"
)

type Room struct {
	core.QObject

	matrix.Room

	_ string `property:"id"`
	_ string `property:"alias"`
	_ string `property:"name"`
	_ string `property:"topic"`
	_ string `property:"avatar"`
}

func (r *Room) Init() {
	r.ConnectId(func() string { return r.Room.ID })
	r.ConnectSetId(func(id string) { r.Room.ID = id })
	r.ConnectAlias(func() string { return r.Room.Alias })
	r.ConnectSetAlias(func(alias string) { r.Room.Alias = alias })
	r.ConnectName(func() string { return r.Room.Name })
	r.ConnectSetName(func(name string) { r.Room.Name = name })
	r.ConnectTopic(func() string { return r.Room.Topic })
	r.ConnectSetTopic(func(topic string) { r.Room.Topic = topic })
	r.ConnectAvatar(func() string { return r.Room.Avatar })
	r.ConnectSetAvatar(func(avatar string) { r.Room.Avatar = avatar })
}

func (r *Room) FromRoom(room *matrix.Room) {
	r.SetId(room.ID)
	r.SetAlias(room.Alias)
	r.SetName(room.Name)
	r.SetTopic(room.Topic)
	r.SetAvatar(room.Avatar)
}
