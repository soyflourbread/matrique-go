package ui

import (
	"log"

	"github.com/therecipe/qt/core"
	"gitlab.com/b0/matrique-go/matrix"
)

type RoomListModel struct {
	core.QAbstractListModel

	_ *Controller `property:"controller"`

	_ func() `constructor:"init"`

	_ map[int]*core.QByteArray `property:"roles"`
	_ []*Room                  `property:"room"`

	_ func(interface{}) `signal:"doResetModel"`

	_ func(int) *Room `slot:"roomAt"`
	_ func(*Room)     `slot:"addRoom"`
	_ func(row int)   `slot:"removeRoom"`
}

func init() {
}

func (m *RoomListModel) init() {
	m.SetRoles(map[int]*core.QByteArray{
		ID:     core.NewQByteArray2("id", len("id")),
		Name:   core.NewQByteArray2("name", len("name")),
		Alias:  core.NewQByteArray2("alias", len("alias")),
		Topic:  core.NewQByteArray2("topic", len("topic")),
		Avatar: core.NewQByteArray2("avatar", len("avatar")),
	})

	m.ConnectData(m.data)
	m.ConnectRowCount(m.rowCount)
	m.ConnectColumnCount(m.columnCount)
	m.ConnectRoleNames(m.roleNames)
	m.ConnectRoomAt(m.roomAt)
	m.ConnectDoResetModel(m.resetModel)
	m.ConnectAddRoom(m.addRoom)
	m.ConnectRemoveRoom(m.removeRoom)

	m.ConnectControllerChanged(func(controller *Controller) {
		m.Controller().ConnectIsLoginChanged(func(isLogin bool) {
			if isLogin {
				go m.fetchModel()
			}
		})
	})
}

func (m *RoomListModel) fetchModel() {
	defer m.Controller().recovery()

	m.Controller().SetBusy(true)

	resp, err := m.Controller().matrixCli.JoinedRooms()
	checkErr(err)

	roomChan := make(chan *matrix.Room)
	defer close(roomChan)
	for _, roomID := range resp.JoinedRooms {
		go func(id string) {
			defer m.Controller().recovery()

			room := matrix.Room{ID: id}
			room.Init(m.Controller().matrixCli)
			roomChan <- &room
		}(roomID)
	}

	var rooms []*matrix.Room
	for i := 0; i < len(resp.JoinedRooms); i++ {
		rooms = append(rooms, <-roomChan)
	}

	m.Controller().SetBusy(false)
	m.DoResetModel(rooms)
}

func (m *RoomListModel) resetModel(roomsInterface interface{}) {
	rooms := roomsInterface.([]*matrix.Room)
	log.Println("Begin resetting roomlistmodel.")
	m.BeginResetModel()

	var qmlRooms []*Room
	for _, room := range rooms {
		qmlRoom := NewRoom(nil)
		qmlRoom.FromRoom(room)
		qmlRooms = append(qmlRooms, qmlRoom)
	}
	m.SetRoom(qmlRooms)
	m.EndResetModel()
	log.Println("Reset complete.")
}

func (m *RoomListModel) roomAt(row int) *Room {
	return m.Room()[row]
}

func (m *RoomListModel) data(index *core.QModelIndex, role int) *core.QVariant {
	if !index.IsValid() {
		return core.NewQVariant()
	}

	if index.Row() >= len(m.Room()) {
		return core.NewQVariant()
	}

	var r = m.Room()[index.Row()]

	switch role {
	case ID:
		return core.NewQVariant14(r.Id())

	case Name:
		return core.NewQVariant14(r.Name())

	case Alias:
		return core.NewQVariant14(r.Alias())

	case Topic:
		return core.NewQVariant14(r.Topic())

	case Avatar:
		return core.NewQVariant14(r.Avatar())

	default:
		return core.NewQVariant()
	}
}

func (m *RoomListModel) rowCount(parent *core.QModelIndex) int {
	return len(m.Room())
}

func (m *RoomListModel) columnCount(parent *core.QModelIndex) int {
	return 1
}

func (m *RoomListModel) roleNames() map[int]*core.QByteArray {
	return m.Roles()
}

func (m *RoomListModel) addRoom(r *Room) {
	m.BeginInsertRows(core.NewQModelIndex(), len(m.Room()), len(m.Room()))
	m.SetRoom(append(m.Room(), r))
	m.EndInsertRows()
}

func (m *RoomListModel) removeRoom(row int) {
	m.BeginRemoveRows(core.NewQModelIndex(), row, row)
	m.SetRoom(append(m.Room()[:row], m.Room()[row+1:]...))
	m.EndRemoveRows()
}
