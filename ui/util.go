package ui

import "github.com/therecipe/qt/core"

const (
	ID = int(core.Qt__UserRole) + 1<<iota
	Name
	Alias
	Topic
	Avatar
)

const (
	Text = int(core.Qt__DisplayRole) + 1<<iota
	AuthorID
)

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}
