package ui

import (
	"fmt"
	"log"
	"time"

	"github.com/matrix-org/gomatrix"
	"github.com/therecipe/qt/core"
)

type Controller struct {
	core.QObject

	matrixCli *gomatrix.Client

	_ string `property:"serverAddr"`
	_ string `property:"userID"`
	_ string `property:"token"`
	_ string `property:"currentAvatar"`
	_ bool   `property:"busy"`
	_ bool   `property:"isLogin"`

	_ func(err string) `signal:"errorOccured"`

	_ func()                       `slot:"login"`
	_ func(string, string, string) `slot:"loginWithCredentials"`
	_ func()                       `slot:"logout"`
	_ func()                       `slot:"getInfo"`

	_ func() `constructor:"init"`
}

func (c *Controller) recovery() {
	if r := recover(); r != nil {
		fmt.Println("recovered:", r)
		c.ErrorOccured(fmt.Sprintf("%v", r))
	}
}

func (c *Controller) init() {
	c.ConnectLogin(c.login)
	c.ConnectLoginWithCredentials(c.loginWithCredentials)
	c.ConnectLogout(c.logout)
	c.ConnectGetInfo(c.getInfo)

	c.SetIsLogin(false)
	c.SetIsLoginDefault(false)
	c.SetBusy(false)
	c.SetBusyDefault(false)

	c.ConnectIsLoginChanged(func(isLogin bool) {
		if c.IsLogin() {
			go func() {
				defer c.recovery()
				avatar, err := c.matrixCli.GetAvatarURL()
				checkErr(err)
				if avatar != "" {
					c.SetCurrentAvatar("image://mxc/" + avatar)
				}
			}()
			c.Sync()
		}
	})
}

func (c *Controller) login() {
	cli, _ := gomatrix.NewClient(c.ServerAddr(), c.UserID(), c.Token())
	c.matrixCli = cli
	c.SetIsLogin(true)
}

func (c *Controller) loginWithCredentials(serverAddr string, username string, password string) {
	cli, _ := gomatrix.NewClient(serverAddr, "", "")
	go func() {
		defer c.recovery()

		resp, err := cli.Login(&gomatrix.ReqLogin{
			Type:     "m.login.password",
			User:     username,
			Password: password,
		})
		checkErr(err)
		cli.SetCredentials(resp.UserID, resp.AccessToken)
		c.SetServerAddr(serverAddr)
		c.SetUserID(resp.UserID)
		c.SetToken(resp.AccessToken)
		c.matrixCli = cli
		c.SetIsLogin(true)
	}()
}

func (c *Controller) logout() {
	log.Println("Removing credentials.")
	c.SetServerAddr("")
	c.SetUserID("")
	c.SetToken("")
}

func (c *Controller) getInfo() {
	log.Println("Get Info:", "Server:", c.ServerAddr(), "ID:", c.UserID(), "Token:", c.Token())
}

func (c *Controller) Sync() {
	log.Println("Start syncing...")
	syncer := c.matrixCli.Syncer.(*gomatrix.DefaultSyncer)
	syncer.OnEventType("m.room.message", func(e *gomatrix.Event) {
		fmt.Println("Message: ", e)
	})

	go func() {
		defer c.recovery()

		for {
			checkErr(c.matrixCli.Sync())
			// Optional: Wait a period of time before trying to sync again.
			time.Sleep(time.Second * 5)
		}
	}()
}
