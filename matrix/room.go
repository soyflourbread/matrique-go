package matrix

import (
	"github.com/matrix-org/gomatrix"
	"log"
	"strings"
)

type Room struct {
	ID     string
	Alias  string
	Name   string
	Topic  string
	Avatar string

	cli *gomatrix.Client
}

func (r *Room) Init(cli *gomatrix.Client) {
	r.SetCli(cli)
	r.FetchAlias()
	r.FetchName()
	r.FetchTopic()
	r.FetchAvatar()
}

func (r *Room) SetCli(cli *gomatrix.Client) {
	r.cli = cli
}

func (r *Room) FetchAlias() {
	type RespRoomAlias struct {
		Alias string `json:"alias"`
	}
	resp := new(RespRoomAlias)
	err := r.cli.StateEvent(r.ID, "m.room.canonical_alias", "", resp)
	if err == nil && resp.Alias != "" {
		r.Alias = resp.Alias
		return
	}
}

func (r *Room) FetchName() {
	type RespRoomName struct {
		Name string `json:"name"`
	}
	resp := new(RespRoomName)
	err := r.cli.StateEvent(r.ID, "m.room.name", "", resp)
	if err == nil && resp.Name != "" {
		r.Name = resp.Name
		return
	}
	respM, _ := r.cli.JoinedMembers(r.ID)
	memberSlice := respM.Joined

	if len(memberSlice) == 2 {
		for eachID, eachMember := range memberSlice {
			if eachID == r.cli.UserID {
				continue
			}
			if eachMember.DisplayName != nil {
				r.Name = *eachMember.DisplayName
				return
			}
			break
		}
	}
}

func (r *Room) FetchTopic() {
	type RespRoomTopic struct {
		Topic string `json:"topic"`
	}
	resp := new(RespRoomTopic)
	err := r.cli.StateEvent(r.ID, "m.room.topic", "", resp)
	if err != nil {
		log.Println("Error when fetching topic of room", r.ID, ":", err.Error())
		return
	}
	r.Topic = resp.Topic
}

func (r *Room) FetchAvatar() {
	type RespRoomAvatar struct {
		Avatar string `json:"url"`
	}
	resp := new(RespRoomAvatar)
	err := r.cli.StateEvent(r.ID, "m.room.avatar", "", resp)
	if err == nil && resp.Avatar != "" {
		r.Avatar = resp.Avatar
		return
	}
	respM, _ := r.cli.JoinedMembers(r.ID)
	memberSlice := respM.Joined

	if len(memberSlice) == 2 {
		for eachID, eachMember := range memberSlice {
			if eachID == r.cli.UserID {
				continue
			}
			if eachMember.AvatarURL != nil {
				r.Avatar = *eachMember.AvatarURL
				return
			}
			break
		}
	}

	r.Avatar = strings.ToUpper(r.Name)
}
