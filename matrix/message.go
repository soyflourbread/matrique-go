package matrix

import (
	"github.com/matrix-org/gomatrix"
	"time"
)

type Message struct {
	EventID   string
	Event     *gomatrix.Event
	AuthorID  string
	Text      string
	Timestamp *time.Time
}
