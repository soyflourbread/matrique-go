# Matrique

Matrique is a glossy client for Matrix, the decentralized instant messaging protocol.

Matrique is written in Golang and QtQuick Controls 2, and tries to be QML-oriented.