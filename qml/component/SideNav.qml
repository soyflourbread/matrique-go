import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.3

Item {
    Rectangle {
        anchors.fill: parent
        color: Material.accent
    }
}
