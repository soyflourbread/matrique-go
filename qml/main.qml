import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.2
import QtGraphicalEffects 1.0
import Qt.labs.settings 1.0
import Qt.labs.platform 1.0 as Platform
import Matrique 0.1

import "qrc:/qml/component"
import "qrc:/qml/form"

ApplicationWindow {
    id: window
    visible: true
    width: 960
    height: 640
    minimumWidth: 320
    minimumHeight: 320
    title: qsTr("Matrique")

    FontLoader { id: materialFont; source: "qrc:/qml/asset/font/material.ttf" }

    Settings {
        id: setting
        property alias serverAddr: matriqueController.serverAddr
        property alias userID: matriqueController.userID
        property alias token: matriqueController.token
    }

    Platform.SystemTrayIcon {
        visible: true
        iconSource: "qrc:/qml/asset/img/icon.png"

        onActivated: {
            window.show()
            window.raise()
            window.requestActivate()
        }
    }

    Controller {
        id: matriqueController
        onErrorOccured: {
            errorDialog.text = err;
            errorDialog.open();
        }

        onServerAddrChanged: imageProvider.setServerAddr(matriqueController.serverAddr)
    }

    Popup {
        property bool busy: matriqueController.busy

        id: busyPopup

        x: (window.width - width) / 2
        y: (window.height - height) / 2
        modal: true
        focus: true

        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent

        BusyIndicator { running: true }

        onBusyChanged: {
            if(busyPopup.busy) { busyPopup.open(); }
            else { busyPopup.close(); }
        }
    }

    Dialog {
        property alias text: errorLabel.text

        id: errorDialog
        width: 360
        modal: true
        title: "ERROR"

        x: (window.width - width) / 2
        y: (window.height - height) / 2

        standardButtons: Dialog.Ok

        Label {
            id: errorLabel
            width: parent.width
            text: "Label"
            wrapMode: Text.Wrap
        }
    }

    Component {
        id: loginPage

        Login { controller: matriqueController }
    }

    Room {
        id: roomPage
        controller: matriqueController
    }

    RowLayout {
        anchors.fill: parent
        spacing: 0

        SideNav {
            id: sideNav
            Layout.preferredWidth: 80
            Layout.fillHeight: true

            ColumnLayout {
                anchors.fill: parent
                spacing: 0

                SideNavButton {
                    contentItem: ImageStatus {
                        width: parent.width
                        height: parent.width
                        source: matriqueController.currentAvatar === "" ? "qrc:/qml/asset/img/avatar.png" : matriqueController.currentAvatar
                        anchors.horizontalCenter: parent.horizontalCenter
                        opaqueBackground: false
                    }

                    page: roomPage
                }

                Rectangle {
                    color: "transparent"
                    Layout.fillHeight: true
                }

                SideNavButton {
                    contentItem: MaterialIcon { icon: "\ue8b8"; color: "white" }

                    onClicked: matriqueController.logout()
                }

                SideNavButton {
                    contentItem: MaterialIcon { icon: "\ue879"; color: "white" }
                    onClicked: Qt.quit()
                }
            }
        }

        StackView {
            id: stackView
            initialItem: roomPage

            Layout.fillWidth: true
            Layout.fillHeight: true
        }
    }

    Component.onCompleted: {
        if (matriqueController.serverAddr != "") {
            console.log("Perform auto-login.");
            matriqueController.login();
        } else {
            stackView.replace(loginPage);
        }
    }
}
