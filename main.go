package main

import (
	"os"

	"gitlab.com/b0/matrique-go/ui"

	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/gui"
	"github.com/therecipe/qt/qml"
)

func main() {
	core.QCoreApplication_SetAttribute(core.Qt__AA_EnableHighDpiScaling, true)
	core.QCoreApplication_SetOrganizationName("ENCOM")

	gui.NewQGuiApplication(len(os.Args), os.Args)

	ui.Controller_QmlRegisterType2("Matrique", 0, 1, "Controller")
	ui.RoomListModel_QmlRegisterType2("Matrique", 0, 1, "RoomListModel")
	ui.MessageEventModel_QmlRegisterType2("Matrique", 0, 1, "MessageEventModel")

	var app = qml.NewQQmlApplicationEngine(nil)

	pro := ui.NewImageProvider(nil)
	pro.Init()
	app.AddImageProvider("mxc", pro.GetProvider())

	app.RootContext().SetContextProperty("imageProvider", pro)

	app.Load(core.NewQUrl3("qrc:/qml/main.qml", 0))

	gui.QGuiApplication_Exec()
}
